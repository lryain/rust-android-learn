use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{WindowBuilder, self},
};

fn main() {
    env_logger::init();

    // 创建了一个窗口，并在用户关闭或按下 escape 键前使其保持打开
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent { 
            window_id, 
            ref event 
        } if window_id == window.id() => match event {
            WindowEvent::CloseRequested | WindowEvent::KeyboardInput { 
                input: KeyboardInput { 
                    state: ElementState::Pressed, 
                    virtual_keycode: Some(VirtualKeyCode::Escape), 
                    .. 
                },
                ..
            } => *control_flow = ControlFlow::Exit,
            _ => {}
        },
        _ => {}
    });
}