struct VertexInput {
    @location(0) position: vec3f,
    // @location(1) color: vec3f,
    @location(1) tex_coords: vec2f,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4f,
    // @location(1) color: vec3f,
    @location(0) tex_coords: vec2f,
};

@vertex
fn vs_main(
    model: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    out.tex_coords = model.tex_coords;
    out.clip_position = vec4f(model.position, 1.0);
    return out;
}

// 变量 t_diffuse 和 s_diffuse 就是所谓的 uniforms。我们将在 相机部分 中进一步讨论 uniforms
// @group(x) 对应于 set_bind_group() 中的第一个参数，@binding(x) 与我们创建绑定组布局和绑定组时指定的 binding 值对应。
@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0) @binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4f{
    // 这将当前片元的颜色设置为棕色。
    return textureSample(t_diffuse, s_diffuse, in.tex_coords);
}