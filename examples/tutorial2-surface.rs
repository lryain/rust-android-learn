use std::f32::consts::E;

use wgpu::{InstanceDescriptor, Surface};
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{self, Window, WindowBuilder},
};

fn main() {
    // 现在 run() 是异步的了，main() 需要某种方式来等待它执行完成。
    // 我们可以使用 tokio 或 async-std 等异步包，但我打算使用更轻量级的 pollster。
    // 在 "Cargo.toml" 中添加以下依赖：pollster = "0.3"
    // 然后我们使用 pollster 提供的 block_on 函数来等待异步任务执行完成：
    pollster::block_on(run());
}

async fn run() {
    env_logger::init();

    // 窗口设置
    // 创建了一个窗口，并在用户关闭或按下 escape 键前使其保持打开
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    //
    let mut state = State::new(&window).await;

    // 事件遍历
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            window_id,
            ref event,
        } if window_id == window.id() => {
            if !state.input(event) {
                match event {
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        // new_inner_size 是 &&mut 类型，因此需要解引用两次
                        state.resize(**new_inner_size);
                    }
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    _ => {}
                }
            }
        }
        Event::RedrawRequested(window_id) if window_id == window.id() => {
            // 我们需再次更新事件循环以调用 render() 函数，还会在它之前先调用 update()。
            state.update();
            match state.render() {
                Ok(_) => {}
                // 当展示平面的上下文丢失，就需重新配置
                Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                // 系统内存不足时，程序应该退出。
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                // 所有其他错误（过期、超时等）应在下一帧解决
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            // 除非我们手动请求，RedrawRequested 将只会触发一次。
            window.request_redraw();
        }
        _ => {}
    });

    //
}

// 建模 State

struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    size: winit::dpi::PhysicalSize<u32>,
}

impl State {
    async fn new(window: &Window) -> Self {
        let size = window.inner_size();

        // instance 是使用 wgpu 时所需创建的第一个实体，其主要用途是创建 Adapter 和 Surface。
        // instance 变量是到 GPU 的 handle
        // Backends::all 对应 Vulkan + Metal + DX12 + 浏览器的 WebGPU
        let instance = wgpu::Instance::new(InstanceDescriptor {
            backends: wgpu::Backends::all(),
            ..Default::default()
        });

        // surface 是我们所绘制窗口的一部分，需要通过它来将内容上屏。另外我们还需要用 surface 来请求 adapter
        let surface = unsafe { instance.create_surface(window).unwrap() };

        // adapter（适配器）是指向实际显卡的一个 handle。我们可以用它获取关于显卡的信息，例如显卡名称与其所适配到的后端等。
        // 稍后我们会用它来创建 Device 和 Queue。
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(), // 默认电源配置
                compatible_surface: Some(&surface),
                // 强制 wgpu 选择一个能在所有硬件上工作的适配器。这通常表明渲染后端将使用一个「软渲染」系统，而非 GPU 这样的硬件。
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        // Device 与 Queue
        // 我们可以用 adapter 来创建 device 和 queue：
        // 你可以用 adapter.features() 或 device.features() 获得设备所支持特性的列表。可以在这里查看完整的特性列表。
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(), // 允许我们指定我们想要的额外特性
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None, // 是否追踪 API 调用路径
            )
            .await
            .unwrap();
        // device.features();
        // adapter.features();
        let caps = surface.get_capabilities(&adapter);
        // 这里我们要为 surface 定义一份配置，以此确定 surface 如何创建其底层的 SurfaceTexture。
        let config = wgpu::SurfaceConfiguration {
            // usage 字段用于定义应如何使用 SurfaceTextures。RENDER_ATTACHMENT 表明纹理将用来上屏（我们将在后面介绍其他的 TextureUsage）。
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            // format 字段定义了 SurfaceTexture 在 GPU 上的存储方式。
            // 不同的显示器会偏好不同的格式。为此我们使用 surface.get_preferred_format(&adapter) 来基于当前显示器计算出对应的最佳格式。
            format: caps.formats[0],
            // width 和 height 是 SurfaceTexture 的宽度和高度（单位为像素）。它们通常应等于窗口的宽度和高度。
            width: size.width,
            height: size.height,
            // present_mode 使用 wgpu::PresentMode 枚举值来确定应如何将 surface 同步到显示器上。
            // 对于我们所选择的 FIFO 选项，其含义是将显示速率限制为显示器的帧速率。实际上这就是 VSync，也是移动设备上最理想的模式。
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        Self {
            surface,
            device,
            queue,
            config,
            size,
        }
    }

    /// 如果要在应用程序中支持调整展示平面的宽高，将需要在每次窗口的大小改变时重新配置 surface。
    /// 这就是我们存储物理 size 和用于配置 surface 的 config 的原因。
    /// 有了这些，实现 resize 函数就非常简单了。
    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        // 调整展示平面的宽高
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
        }
    }

    /// input() 函数返回一个 bool（布尔值），表示一个事件是否已经被处理。
    /// 如果该函数返回 true，主循环就不再继续处理该事件。
    fn input(&mut self, event: &WindowEvent) -> bool {
        false
    }

    fn update(&mut self) {
    }

    /// 这里就是奇迹发生的地方。首先，我们需要获取一个帧（Frame）对象以供渲染：
    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // get_current_texture 函数会等待 surface 提供一个新的 SurfaceTexture。我们将它存储在 output 变量中以便后续使用。
        let output = self.surface.get_current_texture()?;

        // 这一行创建了一个默认设置的纹理视图（TextureView），渲染代码需要利用纹理视图来与纹理交互。
        let texture_view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        // 我们还需要创建一个命令编码器（CommandEncoder）来记录实际的命令发送给 GPU。
        // 大多数现代图形框架希望命令在被发送到 GPU 之前存储在一个命令缓冲区中。命令编码器创建了一个命令缓冲区，然后我们可以将其发送给 GPU。
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        // 现在可以开始执行期盼已久的清屏（用统一的颜色填充指定渲染区域）了。我们需要使用 encoder 来创建渲染通道（RenderPass）。
        // 渲染通道编码所有实际绘制的命令。创建渲染通道的代码嵌套层级有点深，所以在谈论它之前，我先把代码全部复制到这里：
        // 首先，我们来谈谈 encoder.begin_render_pass(...) 周围用 {} 开辟出来的块空间。
        // begin_render_pass() 以可变方式借用了 encoder（又称 &mut self），在释放这个可变借用之前，我们不能调用 encoder.finish()。
        // 这个块空间告诉 rust，当代码离开这个范围时，丢弃其中的任何变量，从而释放 encoder 上的可变借用，并允许我们 finish() 它。
        // 如果你不喜欢 {}，也可以使用 drop(render_pass) 来达到同样的效果。
        {
            let mut _render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                // color_attachments 描述了要将颜色绘制到哪里。我们使用之前创建的纹理视图来确保渲染到屏幕上。
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    // view 字段，用于通知 wgpu 将颜色保存到什么纹理。
                    // 这里我们指定使用 surface.get_current_texture() 创建的 view，这意味着向此附件（Attachment）上绘制的任何颜色都会被绘制到屏幕上。
                    view: &texture_view,
                    resolve_target: None, // 是接收多重采样解析输出的纹理。除非启用了多重采样
                    // 它告诉 wgpu 如何处理屏幕上的颜色（由 view 指定）。
                    ops: wgpu::Operations {
                        // load 字段告诉 wgpu 如何处理存储在前一帧的颜色。目前，我们正在用蓝色清屏。
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        // 告诉 wgpu 是否要将渲染的结果存储到纹理视图后面的纹理上（在这个例子中是 SurfaceTexture）。我们希望存储渲染结果，所以设置为 true
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });
        }
        // submit 命令能接受任何实现了 IntoIter trait 的参数
        // 告诉 wgpu 完成命令缓冲区，并将其提交给 gpu 的渲染队列。
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();
        Ok(())
    }
}
