This tests using `GameActivity` with winit and wgpu.

Although it would have been possible to handle the suspend/resume
lifecycle events with a simpler approach of destroying and
recreating all graphics state, this tries to represent how
lifecycle events could be handled in more realistic applications.

This example also aims to show how it's possible to use Winit
to write portable code that can run on both Android and on desktop
platforms. (enable "desktop" feature to build binary)

```bash
$Env:ANDROID_NDK_HOME="D:\libs\ndk\android-ndk-r25c"
export ANDROID_NDK_HOME="D:\Users\lryain\AppData\Local\Android\Sdk\ndk\25.2.9519653"
export ANDROID_HOME="D:\Users\lryain\AppData\Local\Android\Sdk"

rustup target add aarch64-linux-android
cargo install cargo-ndk

# rustup component add rust-src --toolchain nightly --target aarch64-linux-android
# cargo +nightly ndk -t arm64-v8a -o target/release build -Zbuild-std --verbose --release

cargo ndk -t arm64-v8a -o app/src/main/jniLibs/ build
./gradlew build
./gradlew installDebug
adb shell am start -n co.realfit.agdkwinitwgpu/.MainActivity
```

## 本地测试

cargo add bytemuch --features derive

cargo run --bin test1 --features="desktop"
cargo run --example t4_buffer --features="desktop"
