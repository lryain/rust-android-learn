mod camera;
mod texture;

use std::f32::consts::{E, self};

use camera::{Camera, CameraController, CameraUniform};
use wgpu::{include_wgsl, util::DeviceExt, InstanceDescriptor, Surface};
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{self, Window, WindowBuilder},
};

// wgpu 的世界坐标的 Y 轴朝上，而纹理坐标的 Y 轴朝下
// 通过将每个纹理坐标的 y 坐标替换为 1 - y 来得到纹理的正确朝向
const VERTICES: &[Vertex] = &[
    Vertex {
        position: [-0.086, 0.49, 0.0],
        // color: [1.0, 0.0, 0.0],
        // tex_coords: [0.4131759, -0.99240386],
        tex_coords: [0.4131759, 0.00759614],
    },
    Vertex {
        position: [-0.49, 0.069, 0.0],
        // color: [0.0, 1.0, 0.0],
        // tex_coords: [0.0048659444, 0.56958647],
        tex_coords: [0.0048659444, 0.43041354],
    },
    Vertex {
        position: [-0.21, -0.44, 0.0],
        // color: [0.0, 0.0, 1.0],
        // tex_coords: [0.28081453, 0.05060294],
        tex_coords: [0.28081453, 0.949397],
    },
    Vertex {
        position: [0.35, -0.34, 0.0],
        // color: [0.0, 0.0, 1.0],
        // tex_coords: [0.85967, 0.1526709],
        tex_coords: [0.85967, 0.84732914],
    },
    Vertex {
        position: [0.44, 0.23, 0.0],
        // color: [0.0, 0.0, 1.0],
        // tex_coords: [0.9414737, 0.7347359],
        tex_coords: [0.9414737, 0.2652641],
    },
];

const INDICES: &[u16] = &[0, 1, 4, 1, 2, 4, 2, 3, 4];

const NUM_INSTANCE_PER_ROW: u32 = 10;
const INSTANCE_DISPLACEMENT: glam::Vec3 = glam::Vec3::new(NUM_INSTANCE_PER_ROW as f32 * 0.5, 0.0, NUM_INSTANCE_PER_ROW as f32 * 0.5);

// 实例结构体
struct Instance {
    position: glam::Vec3,
    rotation: glam::Quat,
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct InstanceRaw {
    model: [[f32; 4]; 4],
}

// 需要为 InstanceRaw 创建一个新的顶点缓冲区布局
impl InstanceRaw {
    const ATTRIBS: [wgpu::VertexAttribute; 4] =
    wgpu::vertex_attr_array![5 => Float32x4, 6 => Float32x4, 7 => Float32x4, 8 => Float32x4];

    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<InstanceRaw>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Instance,
            attributes: &Self::ATTRIBS,
        }
    }
}

// impl InstanceRaw {
//     fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
//         use std::mem;
//         wgpu::VertexBufferLayout {
//             array_stride: mem::size_of::<InstanceRaw>() as wgpu::BufferAddress,
//             // step_mode 的值需要从 Vertex 改为 Instance
//             // 这意味着只有着色器开始处理一次新实例化绘制时，才会使用下一个实例数据
//             step_mode: wgpu::VertexStepMode::Instance,
//             attributes: &[
//                 wgpu::VertexAttribute {
//                     offset: 0,
//                     // 虽然顶点着色器现在只使用了插槽 0 和 1，但在后面的教程中将会使用 2、3 和 4
//                     // 此处从插槽 5 开始，确保与后面的教程不会有冲突
//                     shader_location: 5,
//                     format: wgpu::VertexFormat::Float32x4,
//                 },
//                 // mat4 从技术的角度来看是由 4 个 vec4 构成，占用 4 个插槽。
//                 // 我们需要为每个 vec4 定义一个插槽，然后在着色器中重新组装出 mat4。
//                 wgpu::VertexAttribute {
//                     offset: mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
//                     shader_location: 6,
//                     format: wgpu::VertexFormat::Float32x4,
//                 },
//                 wgpu::VertexAttribute {
//                     offset: mem::size_of::<[f32; 8]>() as wgpu::BufferAddress,
//                     shader_location: 7,
//                     format: wgpu::VertexFormat::Float32x4,
//                 },
//                 wgpu::VertexAttribute {
//                     offset: mem::size_of::<[f32; 12]>() as wgpu::BufferAddress,
//                     shader_location: 8,
//                     format: wgpu::VertexFormat::Float32x4,
//                 },
//             ],
//         }
//     }
// }

impl Instance {
    fn to_raw(&self) -> InstanceRaw {
        InstanceRaw {
            model: (glam::Mat4::from_translation(self.position)
                * glam::Mat4::from_quat(self.rotation))
            .to_cols_array_2d(),
        }
    }
}

fn main() {
    // 现在 run() 是异步的了，main() 需要某种方式来等待它执行完成。
    // 我们可以使用 tokio 或 async-std 等异步包，但我打算使用更轻量级的 pollster。
    // 在 "Cargo.toml" 中添加以下依赖：pollster = "0.3"
    // 然后我们使用 pollster 提供的 block_on 函数来等待异步任务执行完成：
    pollster::block_on(run());
}

async fn run() {
    env_logger::init();

    // 窗口设置
    // 创建了一个窗口，并在用户关闭或按下 escape 键前使其保持打开
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    //
    let mut state = State::new(&window).await;

    // 事件遍历
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            window_id,
            ref event,
        } if window_id == window.id() => {
            if !state.input(event) {
                match event {
                    WindowEvent::Resized(physical_size) => {
                        state.resize(*physical_size);
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        // new_inner_size 是 &&mut 类型，因此需要解引用两次
                        state.resize(**new_inner_size);
                    }
                    WindowEvent::CloseRequested
                    | WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Pressed,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    } => *control_flow = ControlFlow::Exit,
                    _ => {}
                }
            }
        }
        Event::RedrawRequested(window_id) if window_id == window.id() => {
            // 我们需再次更新事件循环以调用 render() 函数，还会在它之前先调用 update()。
            state.update();
            match state.render() {
                Ok(_) => {}
                // 当展示平面的上下文丢失，就需重新配置
                Err(wgpu::SurfaceError::Lost) => state.resize(state.size),
                // 系统内存不足时，程序应该退出。
                Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                // 所有其他错误（过期、超时等）应在下一帧解决
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Event::MainEventsCleared => {
            // 除非我们手动请求，RedrawRequested 将只会触发一次。
            window.request_redraw();
        }
        _ => {}
    });

    //
}

// 顶点缓冲区
#[repr(C)]
#[derive(Clone, Copy, Debug, bytemuck::Pod, bytemuck::Zeroable)]
struct Vertex {
    position: [f32; 3],
    // color: [f32; 3],
    tex_coords: [f32; 2],
}

// unsafe impl bytemuck::Pod for Vertex {}
// unsafe impl bytemuck::Zeroable for Vertex {}

impl Vertex {
    const ATTRIBS: [wgpu::VertexAttribute; 2] =
        wgpu::vertex_attr_array![0 => Float32x3, 1 => Float32x2];

    fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &Self::ATTRIBS,
        }
    }
}

// impl Vertex {
//     fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
//         use std::mem;
//         wgpu::VertexBufferLayout {
//             array_stride: mem::size_of::<Vertex>() as wgpu::BufferAddress,
//             step_mode: wgpu::VertexStepMode::Vertex,
//             attributes: &[
//                 wgpu::VertexAttribute {
//                     offset: 0,
//                     shader_location: 0,
//                     format: wgpu::VertexFormat::Float32x3,
//                 },
//                 wgpu::VertexAttribute {
//                     offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
//                     shader_location: 1,
//                     format: wgpu::VertexFormat::Float32x2, // NEW!
//                 },
//             ]
//         }
//     }
// }

// 建模 State

struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    size: winit::dpi::PhysicalSize<u32>,

    // 渲染管线
    render_pipeline: wgpu::RenderPipeline,
    // 顶点缓冲区
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    // num_vertices: u32, =>
    num_indices: u32,

    // 纹理及绑定组
    diffuse_bind_group: wgpu::BindGroup,
    diffuse_texture: texture::Texture,

    // 纹理及绑定组
    diffuse_bind_group2: wgpu::BindGroup,
    diffuse_texture2: texture::Texture,

    show_texture2: bool,

    camera: camera::Camera,
    camera_uniform: CameraUniform,
    camera_buffer: wgpu::Buffer,
    camera_bind_group: wgpu::BindGroup,

    camera_controller: CameraController,

    instances: Vec<Instance>,
    instance_buffer: wgpu::Buffer,
}

impl State {
    async fn new(window: &Window) -> Self {
        let size: winit::dpi::PhysicalSize<u32> = window.inner_size();

        // instance 是使用 wgpu 时所需创建的第一个实体，其主要用途是创建 Adapter 和 Surface。
        // instance 变量是到 GPU 的 handle
        // Backends::all 对应 Vulkan + Metal + DX12 + 浏览器的 WebGPU
        let instance = wgpu::Instance::new(InstanceDescriptor {
            backends: wgpu::Backends::all(),
            ..Default::default()
        });

        // surface 是我们所绘制窗口的一部分，需要通过它来将内容上屏。另外我们还需要用 surface 来请求 adapter
        let surface = unsafe { instance.create_surface(window).unwrap() };

        // adapter（适配器）是指向实际显卡的一个 handle。我们可以用它获取关于显卡的信息，例如显卡名称与其所适配到的后端等。
        // 稍后我们会用它来创建 Device 和 Queue。
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(), // 默认电源配置
                compatible_surface: Some(&surface),
                // 强制 wgpu 选择一个能在所有硬件上工作的适配器。这通常表明渲染后端将使用一个「软渲染」系统，而非 GPU 这样的硬件。
                force_fallback_adapter: false,
            })
            .await
            .unwrap();

        // Device 与 Queue
        // 我们可以用 adapter 来创建 device 和 queue：
        // 你可以用 adapter.features() 或 device.features() 获得设备所支持特性的列表。可以在这里查看完整的特性列表。
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(), // 允许我们指定我们想要的额外特性
                    limits: wgpu::Limits::default(),
                    label: None,
                },
                None, // 是否追踪 API 调用路径
            )
            .await
            .unwrap();
        // device.features();
        // adapter.features();
        let caps = surface.get_capabilities(&adapter);
        // 这里我们要为 surface 定义一份配置，以此确定 surface 如何创建其底层的 SurfaceTexture。
        let config = wgpu::SurfaceConfiguration {
            // usage 字段用于定义应如何使用 SurfaceTextures。RENDER_ATTACHMENT 表明纹理将用来上屏（我们将在后面介绍其他的 TextureUsage）。
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            // format 字段定义了 SurfaceTexture 在 GPU 上的存储方式。
            // 不同的显示器会偏好不同的格式。为此我们使用 surface.get_preferred_format(&adapter) 来基于当前显示器计算出对应的最佳格式。
            format: caps.formats[0],
            // width 和 height 是 SurfaceTexture 的宽度和高度（单位为像素）。它们通常应等于窗口的宽度和高度。
            width: size.width,
            height: size.height,
            // present_mode 使用 wgpu::PresentMode 枚举值来确定应如何将 surface 同步到显示器上。
            // 对于我们所选择的 FIFO 选项，其含义是将显示速率限制为显示器的帧速率。实际上这就是 VSync，也是移动设备上最理想的模式。
            present_mode: wgpu::PresentMode::Fifo,
            alpha_mode: caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        // 载入图片
        let diffuse_bytes = include_bytes!("resource/happy-tree.png");
        let diffuse_bytes2 = include_bytes!("resource/happy-tree2.png");
        let diffuse_texture =
            texture::Texture::from_bytes(&device, &queue, diffuse_bytes, "happy-tree").unwrap();
        let diffuse_texture2 =
            texture::Texture::from_bytes(&device, &queue, diffuse_bytes2, "happy-tree2").unwrap();

        // 绑定组布局 共享布局才能动态切换 绑定组
        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("texture_bind_group_layout"),
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0, // 绑定到 0 资源槽的纹理
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                            view_dimension: wgpu::TextureViewDimension::D2,
                            multisampled: false,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1, // 绑定到 1 资源槽的采样器
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
            });

        // 绑定组
        let diffuse_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("diffuse_bind_group"),
            layout: &texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                },
            ],
        });

        // 绑定组
        let diffuse_bind_group2 = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("diffuse_bind_group"),
            layout: &texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&diffuse_texture2.view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&diffuse_texture2.sampler),
                },
            ],
        });

        // 相机
        let camera = Camera {
            // 将摄像机向上移动 1 个单位，向后移动 2 个单位，+z 朝向屏幕外
            eye: (0.0, 1.0, 2.0).into(),
            // 摄像机看向原点
            target: (0.0, 0.0, 0.0).into(),
            // 定义哪个方向朝上
            up: glam::Vec3::Y,
            aspect: config.width as f32 / config.height as f32,
            fovy: 45.0,
            znear: 0.1,
            zfar: 100.0,
        };

        // 创建一个名为 camera_buffer 的 Uniform 缓冲区
        let mut camera_uniform = CameraUniform::new();
        camera_uniform.update_view_proj(&camera);

        // 相机缓冲区
        let camera_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Camera Buffer"),
            contents: bytemuck::cast_slice(&[camera_uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        // 相机绑定组
        let camera_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                label: Some("camera_bind_group_layout"),
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    // 我们只在顶点着色器中需要虚拟摄像机信息，因为要用它来操作顶点。
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
            });

        // 创建实际绑定组
        let camera_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("camera_bind_group"),
            layout: &camera_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: camera_buffer.as_entire_binding(),
            }],
        });
        // 就像对纹理所做的那样，我们需要在管线布局描述符中注册 camera_bind_group_layout：

        let camera_controller = CameraController::new(0.1);

        // 创建一组 10 行 10 列空间排列均匀的实例数据
        let instances = (0..NUM_INSTANCE_PER_ROW).flat_map(|z| {
            (0..NUM_INSTANCE_PER_ROW).map(move |x| {
                let position = glam::Vec3 {
                    x: x as f32,
                    y: 0.0,
                    z: z as f32,
                } - INSTANCE_DISPLACEMENT;
                let rotation = if position.length().abs() <= std::f32::EPSILON {
                    // 这一行特殊确保在坐标 (0, 0, 0) 处的对象不会被缩放到 0
                    // 因为错误的四元数会影响到缩放
                    glam::Quat::from_axis_angle(glam::Vec3::Z, 0.0)
                } else {
                    glam::Quat::from_axis_angle(position.normalize(), consts::FRAC_PI_4)
                };

                Instance {
                    position, rotation,
                }
            })
        }).collect::<Vec<_>>();

        // 数据已经有了，我们来创建实际的实例缓冲区
        let instance_data = instances.iter().map(Instance::to_raw).collect::<Vec<_>>();
        let instance_buffer = device.create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&instance_data),
                usage: wgpu::BufferUsages::VERTEX,
            }
        );



        // 创建渲染管线
        // 首先载入着色器文件"shader.wgsl"
        // 也可以使用 include_wgsl! 宏作为创建 ShaderModuleDescriptor 的快捷方式
        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("Shader"),
            source: wgpu::ShaderSource::Wgsl(include_str!("shader/shader.wgsl").into()),
        });

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Descriptor"),
                bind_group_layouts: &[&texture_bind_group_layout, &camera_bind_group_layout], // 绑定组
                push_constant_ranges: &[],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("RenderPipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[
                    Vertex::desc(), // 使用了顶点缓冲区
                    InstanceRaw::desc(), // 使用实例绘制
                ],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                unclipped_depth: false,
                polygon_mode: wgpu::PolygonMode::Fill,
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        });

        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(INDICES),
            usage: wgpu::BufferUsages::INDEX,
        });

        let num_indices = INDICES.len() as u32;

        Self {
            surface,
            device,
            queue,
            config,
            size,
            render_pipeline,
            vertex_buffer,
            index_buffer,
            num_indices,
            diffuse_bind_group,
            diffuse_texture,
            diffuse_bind_group2,
            diffuse_texture2,
            show_texture2: false,
            camera,
            camera_uniform,
            camera_buffer,
            camera_bind_group,
            camera_controller,
            instances,
            instance_buffer,
        }
    }

    /// 如果要在应用程序中支持调整展示平面的宽高，将需要在每次窗口的大小改变时重新配置 surface。
    /// 这就是我们存储物理 size 和用于配置 surface 的 config 的原因。
    /// 有了这些，实现 resize 函数就非常简单了。
    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        // 调整展示平面的宽高
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
        }
    }

    /// input() 函数返回一个 bool（布尔值），表示一个事件是否已经被处理。
    /// 如果该函数返回 true，主循环就不再继续处理该事件。
    fn input(&mut self, event: &WindowEvent) -> bool {
        self.camera_controller.process_events(event);

        match event {
            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        state,
                        virtual_keycode: Some(VirtualKeyCode::Space),
                        ..
                    },
                ..
            } => {
                self.show_texture2 = *state == ElementState::Pressed;
                log::debug!("show_texture2: {}", self.show_texture2);
                true
            }
            _ => false,
        }
    }

    fn update(&mut self) {
        self.camera_controller.update_camera(&mut self.camera);
        self.camera_uniform.update_view_proj(&self.camera);
        // uniform 缓冲区中的值需要被更新
        self.queue.write_buffer(
            &self.camera_buffer,
            0,
            bytemuck::cast_slice(&[self.camera_uniform]),
        );
    }

    /// 这里就是奇迹发生的地方。首先，我们需要获取一个帧（Frame）对象以供渲染：
    fn render(&mut self) -> Result<(), wgpu::SurfaceError> {
        // get_current_texture 函数会等待 surface 提供一个新的 SurfaceTexture。我们将它存储在 output 变量中以便后续使用。
        let output = self.surface.get_current_texture()?;

        // 这一行创建了一个默认设置的纹理视图（TextureView），渲染代码需要利用纹理视图来与纹理交互。
        let texture_view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        // 我们还需要创建一个命令编码器（CommandEncoder）来记录实际的命令发送给 GPU。
        // 大多数现代图形框架希望命令在被发送到 GPU 之前存储在一个命令缓冲区中。命令编码器创建了一个命令缓冲区，然后我们可以将其发送给 GPU。
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        // 现在可以开始执行期盼已久的清屏（用统一的颜色填充指定渲染区域）了。我们需要使用 encoder 来创建渲染通道（RenderPass）。
        // 渲染通道编码所有实际绘制的命令。创建渲染通道的代码嵌套层级有点深，所以在谈论它之前，我先把代码全部复制到这里：
        // 首先，我们来谈谈 encoder.begin_render_pass(...) 周围用 {} 开辟出来的块空间。
        // begin_render_pass() 以可变方式借用了 encoder（又称 &mut self），在释放这个可变借用之前，我们不能调用 encoder.finish()。
        // 这个块空间告诉 rust，当代码离开这个范围时，丢弃其中的任何变量，从而释放 encoder 上的可变借用，并允许我们 finish() 它。
        // 如果你不喜欢 {}，也可以使用 drop(render_pass) 来达到同样的效果。
        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                // color_attachments 描述了要将颜色绘制到哪里。我们使用之前创建的纹理视图来确保渲染到屏幕上。
                color_attachments: &[
                    // 这就是片元着色器中 @location(0) 标记指向的颜色附件
                    Some(wgpu::RenderPassColorAttachment {
                        // view 字段，用于通知 wgpu 将颜色保存到什么纹理。
                        // 这里我们指定使用 surface.get_current_texture() 创建的 view，这意味着向此附件（Attachment）上绘制的任何颜色都会被绘制到屏幕上。
                        view: &texture_view,
                        resolve_target: None, // 是接收多重采样解析输出的纹理。除非启用了多重采样
                        // 它告诉 wgpu 如何处理屏幕上的颜色（由 view 指定）。
                        ops: wgpu::Operations {
                            // load 字段告诉 wgpu 如何处理存储在前一帧的颜色。目前，我们正在用蓝色清屏。
                            load: wgpu::LoadOp::Clear(wgpu::Color {
                                r: 0.1,
                                g: 0.2,
                                b: 0.3,
                                a: 1.0,
                            }),
                            // 告诉 wgpu 是否要将渲染的结果存储到纹理视图后面的纹理上（在这个例子中是 SurfaceTexture）。我们希望存储渲染结果，所以设置为 true
                            store: true,
                        },
                    }),
                ],
                depth_stencil_attachment: None,
            });
            // 添加渲染管线 shader
            render_pass.set_pipeline(&self.render_pipeline);
            // 使用绑定组 @group(x) 对应于 set_bind_group() 中的第一个参数
            if (self.show_texture2) {
                render_pass.set_bind_group(0, &self.diffuse_bind_group2, &[]);
            } else {
                render_pass.set_bind_group(0, &self.diffuse_bind_group, &[]);
            }
            render_pass.set_bind_group(1, &self.camera_bind_group, &[]);

            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_vertex_buffer(1, self.instance_buffer.slice(..));

            render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint16);
            // set_vertex_buffer 函数接收两个参数，第一个参数是顶点缓冲区要使用的缓冲槽索引。你可以连续设置多个顶点缓冲区。
            // 第二个参数是要使用的缓冲区的数据片断。你可以在硬件允许的情况下在一个缓冲区中存储尽可能多的对象，所以 slice 允许我们指定使用缓冲区的哪一部分。我们用 .. 来指定整个缓冲区。
            // 告诉 wgpu 用 3 个顶点和 1 个实例（实例的索引就是 @builtin(vertex_index) 的由来）来进行绘制。
            // render_pass.draw(0..self.num_indices, 0..1);
            render_pass.draw_indexed(0..self.num_indices, 0, 0..self.instances.len() as _);
        }

        // submit 命令能接受任何实现了 IntoIter trait 的参数
        // 告诉 wgpu 完成命令缓冲区，并将其提交给 gpu 的渲染队列。
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();
        Ok(())
    }
}
